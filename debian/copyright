Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: chef-server-webui
Source: http://wiki.opscode.com/display/chef

Files: *
Copyright: Copyright 2008-2011 Opscode, Inc.
License: Apache-2
 Licensed under the Apache License, Version 2.0 (the "License"); you
 may not use this file except in compliance with the License. You may
 obtain a copy of the License at
 .
    http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 implied. See the License for the specific language governing
 permissions and limitations under the License.

License: GPL-2
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License version 2 as published by the Free Software Foundation
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Files: public/facebox/*
Copyright: Copyright 2007, 2008 Chris Wanstrath <chris@ozmm.org>
License: MIT

Files: public/javascripts/jquery.localscroll.js
       public/javascripts/jquery.scrollTo.js
Copyright: Copyright (c) 2007-2008 Ariel Flesler - aflesler(at)gmail(dot)com
License: MIT or GPL-2

Files: public/javascripts/jquery-1.5.2.*js
Copyright: Copyright 2011, John Resig,
	   Copyright 2011, The Dojo Foundation
License: MIT or GPL-2

Files: public/javascripts/jquery.jeditable*js
Copyright: Copyright (c) 2006-2009 Mika Tuupola, Dylan Verheul
License: MIT

Files: public/javascripts/jquery.suggest.js
Copyright: Copyright (c) 2007
License: Public Domain
 Feel free to do whatever you want with this file

Files: public/javascripts/jquery.tools.min.js:
       public/javascripts/jquery.tools.js
Copyright: Copyright (c) 2009 Tero Piirainen
License: MIT or GPL-2


Files: public/javascripts/jquery.treeTable.min.js
       stylesheets/jquery.treeTable.css
Copyright: Copyright (c) 2011 Ludo van den Boom, http://cubicphuse.nl
License: MIT or GPL-2

Files: public/javascripts/jquery-ui-1.7.1.custom.min.js
       public/stylesheets/jquery-ui-1.7.1.custom.css
       public/javascripts/jquery-ui.js
Copyright: Copyright (c) 2009 Richard D. Worth and others
License: MIT or GPL-2

Files: public/javascripts/yetii-min.js
Copyright: Copyright (c) kminek.pl & Grzegorz Wójcik
License: BSD
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 * Neither the name of the kminek.pl & Grzegorz Wójcik nor the names
 of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files: public/javascripts/json.js
Copyright: Public Domain
License:
	N/A
