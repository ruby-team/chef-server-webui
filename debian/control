Source: chef-server-webui
Section: ruby
Priority: optional
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Bryan McLellan <btm@loftninjas.org>,
           Tollef Fog Heen <tfheen@debian.org>
Build-Depends: debhelper (>= 9~),
               gem2deb
Standards-Version: 3.9.7
Homepage: http://wiki.opscode.com/display/chef
XS-Ruby-Versions: all

Package: chef-server-webui
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: adduser,
         chef (>= 10.12),
         chef-server-api (>= 10.12),
         merb-core,
         ruby | ruby-interpreter,
         ruby-coderay,
         ruby-json (>= 1.4.6),
         ruby-merb-assets,
         ruby-merb-haml,
         ruby-merb-helpers,
         ruby-merb-param-protection,
         ruby-openid,
         thin,
         ucf,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Chef configuration management system - Web UI
 Chef is a systems integration framework and configuration management system
 written in Ruby. Chef provides a Ruby library and API that can be used to
 bring the benefits of configuration management to an entire infrastructure.
 .
 The Chef Server is a Merb application that provides centralized storage and
 distribution for recipes stored in "cookbooks," management and authentication
 of client nodes and node data, and search indexes for that data.
 .
 This package provides a web UI administrators can use for interacting
 with the Chef server
