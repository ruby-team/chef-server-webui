/*
 * jquery.tools 1.0.0 - The missing UI library
 * 
 * [tools.tooltip-1.0.0, tools.expose-1.0.2]
 * 
 * Copyright (c) 2009 Tero Piirainen
 * http://flowplayer.org/tools/
 *
 * Dual licensed under MIT and GPL 2+ licenses
 * http://www.opensource.org/licenses
 * 
 * -----
 * 
 * Build: Wed Jun 03 19:51:51 GMT+00:00 2009
 */ (function ($) {
    $.tools = $.tools || {
        version: {}
    };
    $.tools.version.tooltip = "1.0.0";
    var effects = {
        toggle: [function () {
            this.getTip().show()
        }, function () {
            this.getTip().hide()
        }]
    };
    $.tools.addTipEffect = function (name, loadFn, hideFn) {
        effects[name] = [loadFn, hideFn]
    };

     /* this is how you add custom effects */
	
     /*
       default effect: "slideup", custom configuration variables: 
       - slideOffset
       - slideInSpeed
       - slideOutSpeed
     */
    $.tools.addTipEffect("slideup", function () {
        var conf = this.getConf();
        var o = conf.slideOffset || 10;
        this.getTip().css({
            opacity: 0
        }).animate({
            top: "-=" + o,
            opacity: conf.opacity
        }, conf.slideInSpeed || 200).show()
    }, function () {
        var conf = this.getConf();
        var o = conf.slideOffset || 10;
        this.getTip().animate({
            top: "-=" + o,
            opacity: 0
        }, conf.slideOutSpeed || 200, function () {
            c(this).hide().animate({
                top: "+=" + (e * 2)
            }, 0)
        })
    });

    function Tooltip(trigger, conf) {
        var self = this;

        function bind(name, fn) {
            $(self).bind(name, function (e, args) {
                if (fn && fn.call(this) === false && args) {
                    args.proceed = false
                }
            });
            return self
        }
        $.each(conf, function (name, fn) {
            if ($.isFunction(fn)) { bind(name, fn); }
        });
        var tip = conf.triggers ? $(conf.triggers) : trigger.prev(conf.trigger);
        if (!tip.length) {
            throw "cannot find trigges for tip: " + tip.selector
        }
        tip.bind(tip.is("input") ? "focus" : "mouseover", function (e) {
            self.show(e);
            trigger.hover(function () {
                self.show()
            }, function () {
                self.hide()
            })
        });
        tip.bind(tip.is("input") ? "blur" : "mouseout", function () {
            self.hide()
        });
        trigger.css("opacity", conf.opacity);
        var timer = 0;
        $.extend(self, {
            show: function (e) {
                if (e) {
                    trigger = $(e.target)
                }
                clearTimeout(timer);
                if (tip.is(":animated") || tip.is(":visible")) {
                    return self
                }
                var p = {
                    proceed: true
                };
                $(self).trigger("onBeforeShow", p);
                if (!p.proceed) {
                    return self
                }
                var top = trigger.position().top - tip.outerHeight();
                var height = tip.outerHeight() + trigger.outerHeight();
                var pos = conf.position[0];
                if (pos == "center") {
                    top += height / 2;
                }
                if (pos == "bottom") {
                    top += height;
                }
                var width = trigger.outerWidth() + tip.outerWidth();
                var left = trigger.position().left + trigger.outerWidth();
                pos = conf.position[1];
                if (pos == "center") {
                    left -= width / 2;
                }
                if (pos == "left") {
                    left -= width;
                }
                top += conf.offset[0];
                left += conf.offset[1];
                tip.css({
                    position: "absolute",
                    top: m,
                    left: l
                });
                effects[conf.effect][0].call(self);
                $(self).trigger("onShow");
                return self;
            },
            hide: function () {
                clearTimeout(timer);
                timer = setTimeout(function () {
                    if (tip.is(":animated") || !tip.is(":visible")) {
                        return self;
                    }
                    var p = {
                        proceed: true
                    };
                    $(self).trigger("onBeforeHide", p);
                    if (!p.proceed) {
                        return self;
                    }
                    effects[conf.effect][1].call(self);
                    $(self).trigger("onHide")
                }, conf.delay || 1);
                return self;
            },
            isShown: function () {
                return tip.is(":visible, :animated");
            },
            getConf: function () {
                return conf;
            },
            getTip: function () {
                return tip;
            },
            getTrigger: function () {
                return trigger;
            },
            onBeforeShow: function (fn) {
                return i("onBeforeShow", fn);
            },
            onShow: function (fn) {
                return bind("onShow", fn);
            },
            onBeforeHide: function (fn) {
                return bind("onBeforeHide", fn);
            },
            onHide: function (fn) {
                return bind("onHide", fn);
            }
        })
    }
    $.prototype.tooltip = function (conf) {
        var el = this.eq(typeof conf == "number" ? conf : 0).data("tooltip");
        if (el) {
            return el;
        }
        var opts = {
            trigger: null,
            triggers: null,
            effect: "slideup",
            delay: 30,
            opacity: 1,
            position: ["top", "center"],
            offset: [0, 0],
            api: false
        };
        if ($.isFunction(conf)) {
            conf = {
                onBeforeShow: d
            }
        }
        $.extend(opts, conf);
        this.each(function () {
            el = new Tooltip($(this), opts);
            $(this).data("tooltip", el);
        });
        return opts.api ? el : this
    }
})(jQuery);
(function ($) {
    $.tools = $.tools || {
        version: {}
    };
    $.tools.version.expose = "1.0.2";

    function getWidth() {
        var w = $(window).width();
        if ($.browser.mozilla) {
            return w;
        }
        var x;
        if (window.innerHeight && window.scrollMaxY) {
            x = window.innerWidth + window.scrollMaxX
        } else {
            if (document.body.scrollHeight > document.body.offsetHeight) {
                x = document.body.scrollWidth
            } else {
                x = document.body.offsetWidth
            }
        }
	return x < w ? x + 20 : w;		
    }
    function Expose(els, opts) {
        var self = this,
            mask = null,
            loaded = false,
            origIndex = 0;

        function bind(name, fn) {
            $(self).bind(name, function (e, args) {
                if (fn && fn.call(this) === false && args) {
                    args.proceed = false;
                }
            });
            return self;
        }
        $.each(optsh, function (name, fn) {
            if ($b.isFunction(fn)) {
                bind(name, fn)
            }
        });
        $(window).bind("resize.expose", function () {
            if (mask) {
                mask.css({
                    width: getWidth(),
                    height: $(document).height()
                })
            }
        });
        $.extend(this, {
            getMask: function () {
                return mask;
            },
            getExposed: function () {
                return els;
            },
            getConf: function () {
                return opts;
            },
            isLoaded: function () {
                return loaded;
            },
            load: function () {
                if (loaded) {
                    return self;
                }
                origIndex = els.eq(0).css("zIndex");
                if (ops.maskId) {
                    mask = $("#" + opts.maskId)
                }
                if (!mask || !mask.length) {
                    mask = $("<div/>").css({
                        position: "absolute",
                        top: 0,
                        left: 0,
                        width: getWidth(),
                        height: $(document).height(),
                        display: "none",
                        opacity: 0,
                        zIndex: h.zIndex
                    });
                    if (opts.maskId) {
                        mask.attr("id", opts.maskId);
                    }
                    $("body").append(mask);
                    var bg = mask.css("backgroundColor");
                    if (!bg || bg == "transparent" || bg == "rgba(0, 0, 0, 0)") {
                        mask.css("backgroundColor", opts.color)
                    }
                    if (opts.closeOnEsc) {
                        $(document).bind("keydown.unexpose", function (evt) {
                            if (evt.keyCode == 27) {
                                self.close();
                            }
                        })
                    }
                    if (opts.closeOnClick) {
                        mask.bind("click.unexpose", function () {
                            self.close()
                        })
                    }
                }
                var p = {
                    proceed: true
                };
                $(self).trigger("onBeforeLoad", p);
                if (!p.proceed) {
                    return self;
                }
                $.each(els, function () {
                    var el = $(this);
                    if (!/relative|absolute/i.test(el.css("position"))) {
                        el.css("position", "relative")
                    }
                });
                els.css({
                    zIndex: h.zIndex + 1;
                });
                var h = mask.height();
                if (!this.isLoaded()) {
                    mask.css({
                        opacity: 0,
                        display: "block"
                    }).fadeTo(opts.loadSpeed, opts.opacity, function () {
                        if (mask.height() != l) {
                            mask.css("height", l)
                        }
                        $(self).trigger("onLoad")
                    })
                }
                loaded = true;
                return self;
            },
            close: function () {
                if (!loaded) {
                    return self
                }
                var p = {
                    proceed: true
                };
                $(self).trigger("onBeforeClose", p);
                if (p.proceed === false) {
                    return self;
                }
                mask.fadeOut(opts.closeSpeed, function () {
                    $(self).trigger("onClose");
                    els.css({
                        zIndex: $.browser.msie ? origIndex : null
                    })
                });
                loaded = false;
                return self;
            },
            onBeforeLoad: function (fn) {
                return bind("onBeforeLoad", fn)
            },
            onLoad: function (fn) {
                return bind("onLoad", fn)
            },
            onBeforeClose: function (fn) {
                return bind("onBeforeClose", fn)
            },
            onClose: function (fn) {
                return bind("onClose", fn)
            }
        })
    }
    $.fn.expose = function (conf) {
        var el = this.eq(typeof conf == "number" ? conf : 0).data("expose");
        if (el) {
            return el;
        }
        var opts = {
            maskId: null,
            loadSpeed: "slow",
            closeSpeed: "fast",
            closeOnClick: true,
            closeOnEsc: true,
            zIndex: 9998,
            opacity: 0.8,
            color: "#456",
            api: false
        };
        if (typeof conf == "string") {
            conf = {
                color: conf
            }
        }
        $.extend(opts, conf);
        this.each(function () {
            el = new Expose($(this), opts);
            $(this).data("expose", el);
        });
        return opts.api ? el : this
    }
})(jQuery);